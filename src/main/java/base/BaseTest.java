package base;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import utils.WebDriverFactory;

import java.util.Date;

import static utils.AutomationProcess.*;

public class BaseTest {

    @BeforeClass
    public static void baseTest() {
        WebTest_Automation_Task_Start();
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver.exe");
        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--disable-notifications","--disable-popup-blocking", "--profile-directory=Profile3",
                "--disable-infobars", "--start-maximized");
        DesiredCapabilities SSLCertificate = DesiredCapabilities.chrome();
        SSLCertificate.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        WebDriver driver = new ChromeDriver(chromeOptions);
        WebTest_Automation_Task_Start_Browser("Chrome");
        driver.manage().window().maximize();
        WebDriverFactory.setDriver(driver);
        long time = System.currentTimeMillis();
        WebDriverFactory.setDate(new Date(time));
        WebDriverFactory.setTime(time);
        WebTest_Automation_Task_Start_Test("Chrome");
        WebTest_Automation_Task_TestStart();
    }

    @AfterClass()
    public static void tearDown() {
        if (WebDriverFactory.getDriver() != null)
            WebDriverFactory.getDriver().quit();

        long testTime = System.currentTimeMillis() - WebDriverFactory.getTime();
        WebTest_Automation_Task_Finish_Browser(testTime, "Chrome");
    }

}
