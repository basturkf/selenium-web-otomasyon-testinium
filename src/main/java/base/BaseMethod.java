package base;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import utils.LogUtil;
import utils.WebDriverFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BaseMethod {

    private final WebDriverWait driverWait;
    private final WebDriverWait driverCheckWait;

    public BaseMethod() {
        this.driverWait = new WebDriverWait(WebDriverFactory.getDriver(), 30);
        this.driverCheckWait = new WebDriverWait(WebDriverFactory.getDriver(), 5);
    }

    public void goPage(String link) {
        WebDriverFactory.getDriver().get(link);
    }

    public String getTitle() {
        return WebDriverFactory.getDriver().getTitle();
    }

    public WebElement waitForElementBy(By cssSelector) {
        return driverWait.until(ExpectedConditions.visibilityOfElementLocated(cssSelector));
    }

    public void waitCheckForElementBy(By cssSelector) {
        driverWait.until(ExpectedConditions.visibilityOfElementLocated(cssSelector));
    }

    public List<WebElement> waitForElementsBy(By cssSelector) {
        return driverWait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(cssSelector));
    }

    public void clickElement(By locator) {
        waitForElementBy(locator).click();
    }

    public void sendKeys(By locator, String content) {
        waitForElementBy(locator).sendKeys(content);
    }

    public void sendKeys(By locator, Keys keys) {
        waitForElementBy(locator).sendKeys(keys);
    }

    public boolean isVisibleElement(By locator) {
        try {
            waitCheckForElementBy(locator);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void scrollJSElementBy(By cssSelector) {
        WebElement element = waitForElementBy(cssSelector);
        JavascriptExecutor js = (JavascriptExecutor) WebDriverFactory.getDriver();
        js.executeScript("arguments[0].scrollIntoView();", element);
    }

    public void scrollJSElement(WebElement element) {
        JavascriptExecutor js = (JavascriptExecutor) WebDriverFactory.getDriver();
        js.executeScript("arguments[0].scrollIntoView();", element);
    }

    public String getText(By locator) {
        return waitForElementBy(locator).getText();
    }

    public void selectItemIndex(By locator, int index) {
        Select dropdown = new Select(waitForElementBy(locator));
        dropdown.selectByIndex(index);
    }

    public List<WebElement> getListAllElement(By locator) {
        return waitForElementsBy(locator);
    }

    public void hoverToElement(By hoverSelector) {
        Actions action = new Actions(WebDriverFactory.getDriver());
        WebElement hoverElement = WebDriverFactory.getDriver().findElement(hoverSelector);
        action.moveToElement(hoverElement).perform();
    }

    public void JSClickElement(WebElement element) {
        JavascriptExecutor executor = (JavascriptExecutor) WebDriverFactory.getDriver();
        executor.executeScript("arguments[0].click();", element);
    }

    public void waitForPageLoad() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driverWait.until(driver -> {
            LogUtil.debug(" Current Window State       : "
                    + ((JavascriptExecutor) driver).executeScript("return document.readyState"));
            return String
                    .valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
                    .equals("complete");
        });
    }

    public void writeToFile(String productName, String productCost) {
        String str = "Product Name:" + productName + "\n" +
                "Product Cost:" + productCost;

        SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd_HH-mm");
        String time = formatterDate.format(WebDriverFactory.getDate());

        String productInfoLoc = System.getProperty("user.dir") + "\\ProductInfo";
        File fileProductInfo = new File(productInfoLoc);
        fileProductInfo.mkdir();

        String timeLoc = System.getProperty("user.dir") + "\\ProductInfo\\" + time;
        File fileTime = new File(timeLoc);
        fileTime.mkdir();

        String productLoc = System.getProperty("user.dir") + "\\ProductInfo\\" +
                time + "\\product.txt";
        try {
            File myObj = new File(productLoc);
            boolean isExist = myObj.createNewFile();

            FileWriter myWriter = new FileWriter(productLoc);
            myWriter.write(str);
            myWriter.close();
        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    public List<String> readToFile() {
        SimpleDateFormat formatterDate = new SimpleDateFormat("yyyy-MM-dd_HH-mm");
        String time = formatterDate.format(WebDriverFactory.getDate());

        String productLoc = System.getProperty("user.dir") + "\\ProductInfo\\" +
                time + "\\product.txt";

        List<String> result = new ArrayList<>();
        try {
            File myObj = new File(productLoc);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                if (data.contains("Product Name:"))
                    result.add(data.substring(data.indexOf("Product Name:") + 13));
                else if (data.contains("Product Cost:"))
                    result.add(data.substring(data.indexOf("Product Cost:") + 13));
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return result;
    }


}
