package pages;

import base.BaseMethod;
import org.openqa.selenium.By;

import static utils.AutomationProcess.WebTest_Automation_Task_Page_Func;

public class ProductPage extends BaseMethod {
    private final By TXT_PRODUCT_NAME = By.xpath("//h1[@id='sp-title']");
    private final By TXT_PRODUCT_COST = By.xpath("//span[@id='sp-price-highPrice']");
    private final By BTN_ADD_TO_BASKET = By.xpath("//button[@id='add-to-basket']");

    public void getProductInfo() {
        WebTest_Automation_Task_Page_Func(5, "Open Basket Page");
        String productName = getText(TXT_PRODUCT_NAME).trim();
        String productCost = getText(TXT_PRODUCT_COST).trim();

        writeToFile(productName,productCost);
    }

    public void addProductToBasket() {
        WebTest_Automation_Task_Page_Func(6, "Open Basket Page");
        scrollJSElementBy(BTN_ADD_TO_BASKET);
        clickElement(BTN_ADD_TO_BASKET);
        waitForPageLoad();
    }
}
