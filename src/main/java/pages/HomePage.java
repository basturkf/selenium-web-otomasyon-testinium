package pages;

import base.BaseMethod;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import java.util.List;
import java.util.Random;

import static utils.AutomationProcess.WebTest_Automation_Task_Page_Func;

public class HomePage extends BaseMethod {
    private final By BTN_PROFIL = By.xpath("//div[@title='Giriş Yap']");
    private final By BTN_LOGIN = By.xpath("//a[contains(@href,'uye-girisi')]");
    private final By TXT_PROFIL = By.xpath("//div[@title='Hesabım']");
    private final By INP_SEARCH = By.xpath("//input[@name='k']");
    private final By DV_PAGE_NUMBER = By.xpath("//div[@id='best-match-right']//ul[@class='clearfix']");
    private final By LST_PAGE_NUMBER = By.xpath("//div[@id='best-match-right']//ul[@class='clearfix']//li");
    private final By LST_PRODUCT = By.xpath("//ul[@class='catalog-view clearfix products-container']//li");

    private final By BTN_HOVER_BASKET = By.xpath("//div[@class='basket-container robot-header-iconContainer-cart']");
    private final By BTN_BASKET = By.xpath("//a[contains(@href,'sepetim')]");

    public void openPage() {
        WebTest_Automation_Task_Page_Func(1, "Open Page");
        goPage("https://www.gittigidiyor.com/");
    }

    public void checkItOpen() {
        WebTest_Automation_Task_Page_Func(2, "Check It Open");
        waitForPageLoad();
        String title = getTitle();
        Assert.assertTrue("Not Open Page!", title.contains("GittiGidiyor"));
    }

    public void clickLogin() {
        WebTest_Automation_Task_Page_Func(1, "Click Login");
        hoverToElement(BTN_PROFIL);
        clickElement(BTN_LOGIN);
    }

    public void checkLogin() {
        WebTest_Automation_Task_Page_Func(5, "Check Login");
        boolean result = isVisibleElement(TXT_PROFIL);
        Assert.assertTrue("Not Login!", result);
    }

    public void searchProduct(String name) {
        WebTest_Automation_Task_Page_Func(1, "Search Product");
        sendKeys(INP_SEARCH, name);
        sendKeys(INP_SEARCH, Keys.ENTER);
        waitForPageLoad();
    }

    public void goPageFromResult(int pageNumber) {
        WebTest_Automation_Task_Page_Func(2, "Go Page From Result");
        scrollJSElementBy(DV_PAGE_NUMBER);
        List<WebElement> elementList = getListAllElement(LST_PAGE_NUMBER);
        elementList.get(pageNumber - 1).click();
        waitForPageLoad();
    }

    public void checkOpenNewPage(int pageNumber) {
        WebTest_Automation_Task_Page_Func(3, "Check Open New Page");
        scrollJSElementBy(DV_PAGE_NUMBER);
        List<WebElement> elementList = getListAllElement(LST_PAGE_NUMBER);
        String value = elementList.get(pageNumber).getAttribute("class");
        Assert.assertEquals("Not Open 2. Page!", "selected", value);
    }

    public void selectRandomProduct() {
        WebTest_Automation_Task_Page_Func(4, "Select Random Product");
        List<WebElement> elementList = getListAllElement(LST_PRODUCT);
        int rand_product = new Random().nextInt(elementList.size());
        WebElement element = elementList.get(rand_product);
        scrollJSElement(element);
        JSClickElement(element);
        waitForPageLoad();
    }

    public void openBasketPage() {
        WebTest_Automation_Task_Page_Func(1, "Open Basket Page");
        hoverToElement(BTN_HOVER_BASKET);
        clickElement(BTN_BASKET);
        waitForPageLoad();
    }
}
