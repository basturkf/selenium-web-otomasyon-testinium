package pages;

import base.BaseMethod;
import org.junit.Assert;
import org.openqa.selenium.By;

import java.util.List;

import static utils.AutomationProcess.WebTest_Automation_Task_Page_Func;

public class BasketPage extends BaseMethod {
    private final By TXT_PRODUCT_NAME = By.xpath("//div[contains(@class,'cart-group-item product-item-box product')]//h2");
    private final By TXT_PRODUCT_COST = By.xpath("//div[contains(@class,'cart-group-item product-item-box product')]//div[@class='total-price']");
    private final By BTN_PRODUCT_AMOUNT = By.xpath("//select[@class='amount']");
    private final By BTN_PRODUCT_REMOVE = By.xpath("//div[contains(@class,'cart-group-item product-item-box product')]//a[@title='Sil']");
    private final By DV_PRODUCT = By.xpath("//div[@class='product-group-box']");

    public void checkProductInfo() {
        WebTest_Automation_Task_Page_Func(2, "Check Product Info");
        List<String> productInfo = readToFile();
        String productName = getText(TXT_PRODUCT_NAME).trim();
        String productCost = getText(TXT_PRODUCT_COST).trim();

        Assert.assertEquals("Not the correct product name!", productInfo.get(0), productName);
        Assert.assertEquals("Not the correct product cost!", productInfo.get(1), productCost);

    }

    public void increaseProductNumber(int index) {
        WebTest_Automation_Task_Page_Func(3, "Increase Product Number");
        selectItemIndex(BTN_PRODUCT_AMOUNT, index - 1);
        waitForPageLoad();
    }

    public void checkProductNumber(int index) {
        WebTest_Automation_Task_Page_Func(4, "Check Product Number");
        String value = waitForElementBy(BTN_PRODUCT_AMOUNT).getAttribute("data-value");
        Assert.assertEquals("Not the correct product name!", String.valueOf(index), value);
    }

    public void removeProduct() {
        WebTest_Automation_Task_Page_Func(5, "Remove Product");
        clickElement(BTN_PRODUCT_REMOVE);
        waitForPageLoad();
    }

    public void checkEmptyBasket() {
        WebTest_Automation_Task_Page_Func(6, "Check Empty Basket");
        boolean result = isVisibleElement(DV_PRODUCT);
        Assert.assertFalse("The product is not deleted!", result);
    }

}
