package pages;

import base.BaseMethod;
import org.openqa.selenium.By;

import static utils.AutomationProcess.WebTest_Automation_Task_Page_Func;

public class LoginPage extends BaseMethod {
    public final By INP_USERNAME = By.xpath("//input[@name='kullanici']");
    public final By INP_PASSWORD = By.xpath("//input[@name='sifre']");
    public final By BTN_LOGIN = By.xpath("//input[@id='gg-login-enter']");

    public void setUsername(String username) {
        WebTest_Automation_Task_Page_Func(2, "Set Username");
        sendKeys(INP_USERNAME, username);
    }

    public void setPassword(String password) {
        WebTest_Automation_Task_Page_Func(3, "Set Password");
        sendKeys(INP_PASSWORD, password);
    }

    public void clickLogin() {
        WebTest_Automation_Task_Page_Func(4, "Click Login");
        clickElement(BTN_LOGIN);
        waitForPageLoad();
    }

}
