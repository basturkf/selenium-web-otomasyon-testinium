package utils;

public class AutomationProcess {

    public static void WebTest_Automation_Task_Start() {
        LogUtil.info("1- Gittigidiyor's web test scenario begins.");
    }

    public static void WebTest_Automation_Task_Start_Browser(String driverName) {
        LogUtil.info("2- Browser Name: " + driverName + " is getting ready to run.");
    }

    public static void WebTest_Automation_Task_Start_Test(String driverName) {
        LogUtil.info("3- Browser Name: " + driverName + ": Gittigidiyor tests started.");
    }

    public static void WebTest_Automation_Task_TestStart() {
        LogUtil.info("4- Starting test scenarios in Gittigidiyor Tests");
    }

    public static void WebTest_Automation_Task_Test(int index, String scenarioName) {
        LogUtil.info(" \t4." + index + "- " + scenarioName + " test scenario in Gittigidiyor Tests");
    }

    public static void WebTest_Automation_Task_Create_Page(String scenarioName) {
        LogUtil.info(" \t\t" + scenarioName + " test started for Gittigidiyor Tests");
    }

    public static void WebTest_Automation_Task_Finish_Page(String scenarioName) {
        LogUtil.info(" \t\t" + scenarioName + " test completed for Gittigidiyor Tests\n");
    }

    public static void WebTest_Automation_Task_Page_Func(int stepCount, String funcMessage) {
        LogUtil.info(" \t\t\t " + stepCount + ". Step: " + funcMessage);
    }

    public static void WebTest_Automation_Task_Finish_Browser(long testTime, String driverName) {
        LogUtil.info("5- Test Complete Time:" + testTime + ", Browser Name: " + driverName + " is closed\n\n");
    }

}
