package utils;

import org.openqa.selenium.WebDriver;

import java.util.Date;

public class WebDriverFactory {
    private static final ThreadLocal<WebDriver> driverThread = new ThreadLocal<>();
    private static final ThreadLocal<Date> dateThreadLocal = new ThreadLocal<>();
    private static final ThreadLocal<Long> timeThreadLocal = new ThreadLocal<>();

    public static synchronized void setDriver(WebDriver driver){
        driverThread.set(driver);
    }

    public static synchronized WebDriver getDriver(){
        return driverThread.get();
    }

    public static synchronized void setDate(Date date){
        dateThreadLocal.set(date);
    }

    public static synchronized Date getDate(){
        return dateThreadLocal.get();
    }

    public static synchronized void setTime(long date){
        timeThreadLocal.set(date);
    }

    public static synchronized long getTime(){
        return timeThreadLocal.get();
    }
}
