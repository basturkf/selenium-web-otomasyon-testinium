# Web Test Automation Task

Bu projede Testinium tarafından verilen task oluşturulmuş olup senaryoların Page Object Model kullanılarak koşulması sağlanmıştır. Test projesinin başlatılması için `src\test\java\Gittigidiyor.java` dosyasını JUnit ile çalıştırılmalıdır. Bu projede kullanılanlar:
- Java
- Maven
- Selenium
- Junit
- Log4J

## Proje Özellikleri
* Webdriver ları projede eklenmiştir. Versiyon uyuşmazlığında webdriverlar aynı isim ile src\main\resources\ altında güncellenmelidir.
* Web testi sırasında gerekli olan parametreler(Kullanıcı bilgileri, Arama içeriği) `src\main\java\data\DataManager.java` altında Enum içerikleri düzenlenmeldir.

            EMAIL("email@gmail.com"),
            PASSWORD("pasword"),
            PRODUCT("Bilgisayar"),
            PAGE_NUMBER("2"),
            INCREASE_NUMBER("2")
        
* Projenin log kaydı tutulmaktadır. Log kaydı error, info ve benzeri başlıklar için ayrı ayrı tutulmaktadır. Log kayıtları `Logs/` altında zamana göre tutulmaktadır.
* Projede test sırasında ürün bilgileri dosyaya yazılmaktadır. Bu kayıtlara `ProductInfo/` altında zamana göre tutulmaktadır.

## Testinium Task İsterleri

- www.gittigidiyor.com sitesi açılır.
- Ana sayfanın açıldığı kontrol edilir. Siteye login olunur
- Login işlemi kontrol edilir.
- Arama kutucuğuna bilgisayar kelimesi girilir.
- Arama sonuçları sayfasından 2.sayfa açılır.
- 2.sayfanın açıldığı kontrol edilir.
- Sonuca göre sergilenen ürünlerden rastgele bir ürün seçilir.
- Seçilen ürünün ürün bilgisi ve tutar bilgisi txt dosyasına yazılır.
- Seçilen ürün sepete eklenir.
- Ürün sayfasındaki fiyat ile sepette yer alan ürün fiyatının doğruluğu karşılaştırılır.
- Adet arttırılarak ürün adedinin 2 olduğu doğrulanır.
- Ürün sepetten silinerek sepetin boş olduğu kontrol edilir.

## Test Log Çıktısı

        1- Gittigidiyor's web test scenario begins.
        2- Browser Name: Chrome is getting ready to run.
        3- Browser Name: Chrome: Gittigidiyor tests started.
        4- Starting test scenarios in Gittigidiyor Tests
            4.1- Open Website test scenario in Gittigidiyor Tests
                Open Website test started for Gittigidiyor Tests
                    1. Step: Open Page
                    2. Step: Check It Open
                Open Website test completed for Gittigidiyor Tests

            4.2- Login Website test scenario in Gittigidiyor Tests
                Login Website test started for Gittigidiyor Tests
                    1. Step: Click Login
                    2. Step: Set Username
                    3. Step: Set Password
                    4. Step: Click Login
                    5. Step: Check Login
                Login Website test completed for Gittigidiyor Tests

            4.3- Search Product test scenario in Gittigidiyor Tests
                Search Product test started for Gittigidiyor Tests
                    1. Step: Search Product
                    2. Step: Go Page From Result
                    3. Step: Check Open New Page
                    4. Step: Select Random Product
                    5. Step: Open Basket Page
                    6. Step: Open Basket Page
                Search Product test completed for Gittigidiyor Tests

            4.4- Cart Operations test scenario in Gittigidiyor Tests
                Cart Operations test started for Gittigidiyor Tests
                    1. Step: Open Basket Page
                    2. Step: Check Product Info
                    3. Step: Increase Product Number
                    4. Step: Check Product Number
                    5. Step: Remove Product
                    6. Step: Check Empty Basket
                Cart Operations test completed for Gittigidiyor Tests

        5- Test Complete Time:78022, Browser Name: Chrome is closed





